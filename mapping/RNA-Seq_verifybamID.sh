declare x=0 
cd ~/directory of fastq files

while read -r -a line; do
echo ${line[0]}
echo ${line[1]}

DIR=~/directory of fastq files

for NAME in $(find . -name ${line[0]}'_1.fastq.gz' -printf "%f\n" | sed 's/_1.fastq.gz//'); do
for ADDRESS in $(find . -name $NAME'_1.fastq.gz' | sed 's/_1.fastq.gz//'); do 

echo "$NAME"
echo "$ADDRESS" 

p1='_1.fastq.gz'
p2='_2.fastq.gz'

if [ "$x" -eq 0 ]; then 
    var1="$ADDRESS"; fi
if [ "$x" -eq 1 ]; then 
    var2="$ADDRESS"; fi

x=$((x + 1))

if [ "$x" -eq 2 ]; then

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar 
#$ -m eas

module add picard-tools

cd ~/directory of fastq files

#-- create index for bam files
BuildBamIndex I='$DIR$NAME'_nodup_properPairs_NH.bam; 

#-- verifyBamID
~/verifyBamID.20120620 --vcf ~/genotype_profile_samples.vcf --bam' $DIR$NAME'_nodup_properPairs_NH.bam --maxDepth 1000 --precise --best --out' $DIR$NAME'.selfSM' $DIR$NAME'.depthSM' $DIR$NAME'.selfRG' $DIR$NAME'.depthRG' $DIR$NAME'.bestSM' $DIR$NAME'.bestRG --verbose' > ~/'Shell script for parallel submission'$NAME.sh

x=0
fi

done
done
done < '~/meta_data.txt'   # col1: name of file, col2: name of sample

