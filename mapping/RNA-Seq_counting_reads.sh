
DIR=~       # input directory 
OUTPUT=~    # output directory

cd $DIR

for NAME in $(find . -name "*.bam" -printf "%f\n" | sed 's/.bam//'); do
for ADDRESS in $(find . -name $NAME'.bam'); do 

echo "$NAME"
echo "$ADDRESS"

echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M username
#$ -m eas

module add HTSeq/0.6.1p1
module add python

cd ~

python -m HTSeq.scripts.count --format=bam --minaqual=0 --stranded=no --type=exon --mode=union' $ADDRESS '~/GRCh38.gtf > ~/'$NAME'_gene_read_count.txt' > $OUTPUT'CD_'$NAME'.sh'

done 
done



